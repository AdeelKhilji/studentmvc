/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student;

/**
 *
 * @author Adeel Khilji
 */
public class Student extends Person
{
    private String id;
    
    protected Student(String name, String id)
    {
        super(name);
        this.id = id;
    }
    
    public String getId()
    {
        return this.id;
    }
}