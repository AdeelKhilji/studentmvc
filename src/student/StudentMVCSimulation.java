/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student;

/**
 *
 * @author Adeel Khilji
 */
public class StudentMVCSimulation 
{
    public static void main(String[] args)
    {
        Student model;
        StudentView view;
        StudentController controller;
        
        model = new Student("John","155167889");
        view = new StudentView();
        controller = new StudentController(model, view);
        
        controller.updateView();
        System.out.println();
        
        model = new Student("Jane", "552164789");
        view = new StudentView();
        controller = new StudentController(model,view);
        
        controller.updateView();
        System.out.println();
        
    }
}
